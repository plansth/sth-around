var gulp = require('gulp'),
    less = require('gulp-less'),
    path = require('path'),
    minifyCSS = require('gulp-minify-css'),
    watch = require('gulp-watch'),
    concat = require('gulp-concat');

gulp.task('less', function () {
    return gulp.src('./frontend/less/app.less')
        .pipe(less({
            paths: [path.join(__dirname, 'less', 'includes')]
        }))
        .pipe(minifyCSS())
        .pipe(gulp.dest('./public/css'));
});

gulp.task('concatjs', function () {
    return gulp.src('./frontend/js/**/*.js')
        .pipe(concat('app.js'))
        .pipe(gulp.dest('./public/'));
});


gulp.task('watch', function () {
    gulp.watch('./frontend/less/**/*.less', ['less']);
    gulp.watch('./frontend/js/**/*.js', ['concatjs']);
});